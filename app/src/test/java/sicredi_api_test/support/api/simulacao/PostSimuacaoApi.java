package sicredi_api_test.support.api.simulacao;

import org.apache.http.HttpStatus;
import sicredi_api_test.support.domain.Simulacao;

import static io.restassured.RestAssured.given;

public class PostSimuacaoApi {

    private static final String SIMULACAO_ENDPOINT = "simulacoes";

    public void createSimulacao(Simulacao simulacao) {
        criarSimulacao(simulacao, SIMULACAO_ENDPOINT, HttpStatus.SC_CREATED);
    }

    public void createSimulacaoDuplicada(Simulacao simulacao) {

        criarSimulacao(simulacao, SIMULACAO_ENDPOINT, HttpStatus.SC_CONFLICT);
    }

    public void createSimulacaoInvalida(Simulacao simulacao) {
        criarSimulacao(simulacao, SIMULACAO_ENDPOINT, HttpStatus.SC_BAD_REQUEST);
    }

    private void criarSimulacao(Simulacao simulacao, String endPoint, int httpStatus) {
        given().
                body(simulacao).
                when().
                post(endPoint).
                then().
                statusCode(httpStatus);
    }
}
