package sicredi_api_test.support.api.simulacao;
import org.apache.http.HttpStatus;
import sicredi_api_test.support.domain.Simulacao;

import static io.restassured.RestAssured.given;


public class GetSimulacaoApi {

    private static final String CPF_ENDPOINT = "simulacoes/{cpf}";

    public String returnSimulacaoByCpf(Simulacao simulacao) {
        return given().
                pathParam("cpf", simulacao.getCpf()).
                when().
                get(CPF_ENDPOINT).
                thenReturn().
                path("cpf");
    }

    public String setIdByCpf(Simulacao simulacao) {
         String id = given().
                pathParam("cpf", simulacao.getCpf()).
                when().
                get(CPF_ENDPOINT).
                thenReturn().
                path("id").toString();
        return id;
    }

    public void findSimulacaoNaoCadastradaByCpf(Simulacao simulacao) {
        given().
            pathParam("cpf", simulacao.getCpf()).
        when().
            get(CPF_ENDPOINT).
        then().
            statusCode(HttpStatus.SC_NOT_FOUND);
    }


}
