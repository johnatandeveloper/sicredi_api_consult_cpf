package sicredi_api_test.support.api.simulacao;

import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import sicredi_api_test.support.domain.Simulacao;

import static io.restassured.RestAssured.given;

public class DeleteSimulacaoApi {

    private static final String ID_ENDPOINT = "simulacoes/{id}";

    private GetSimulacaoApi getSimulacaoApi = new GetSimulacaoApi();

    public void deletarSimulacaoById(Simulacao simulacao){
        given().
                pathParam("id", getSimulacaoApi.setIdByCpf(simulacao)).
                when().
                delete(ID_ENDPOINT).
                then().
                statusCode(HttpStatus.SC_NO_CONTENT);
    }
}
