package sicredi_api_test.support.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Simulacao {

    @Builder.Default
    private String nome = "Joao Test";
    @Builder.Default
    private String cpf = "00002311505";
    @Builder.Default
    private String email = "teste@teste.com.br";
    @Builder.Default
    private double valor = 1000;
    @Builder.Default
    private int parcelas = 2;
    @Builder.Default
    private boolean seguro = true;


}
