package sicredi_api_test.steps;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import sicredi_api_test.support.api.simulacao.DeleteSimulacaoApi;
import sicredi_api_test.support.api.simulacao.GetSimulacaoApi;
import sicredi_api_test.support.api.simulacao.PostSimuacaoApi;
import sicredi_api_test.support.domain.Simulacao;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class PostSimulacaoStepDefinitions {

    private Simulacao expectedSimulacao;
    private final PostSimuacaoApi postSimuacaoApi;
    private GetSimulacaoApi getSimulacaoApi;


    public PostSimulacaoStepDefinitions() {
        postSimuacaoApi = new PostSimuacaoApi();
        getSimulacaoApi = new GetSimulacaoApi();
    }

    @Quando("crio uma simuacao")
    public void crio_uma_simuacao() {
        expectedSimulacao = Simulacao.builder().build();

        postSimuacaoApi.createSimulacao(expectedSimulacao);
    }

    @Entao("devo pesquisar e verificar se o CPF esta cadastrado")
    public void o_sistema_deve_salvar_e_retornar_a_mesma() {
        String actualSimulacao = getSimulacaoApi.returnSimulacaoByCpf(expectedSimulacao);

        assertThat(actualSimulacao, is(expectedSimulacao.getCpf()));
    }

    @Quando("nao consigo criar uma simulacao com CPF ja cadastrado")
    public void nao_crio_uma_simulacao_com_cpf_duplicado() {
        expectedSimulacao = Simulacao.builder().cpf("66414919004").build();
        postSimuacaoApi.createSimulacaoDuplicada(expectedSimulacao);
    }

    @Quando("nao consigo criar uma simulacao com CPF invalido")
    public void nao_consigo_criar_uma_simulacao_com_cpf_invalido() {
        expectedSimulacao = Simulacao.builder().cpf("000.bbb.000-09").build();
        postSimuacaoApi.createSimulacaoInvalida(expectedSimulacao);
    }

    @Entao("devo pesquisar e verificar se a simulacao nao foi cadastrado")
    public void devo_pesquisar_e_verificar_se_a_simulacao_nao_foi_cadastrado() {
        getSimulacaoApi.findSimulacaoNaoCadastradaByCpf(expectedSimulacao);
    }

    @Quando("nao consigo criar uma simulacao com email invalido")
    public void nao_consigo_criar_uma_simulacao_com_email_invalido() {
        expectedSimulacao = Simulacao.builder().cpf("00000000090").
                email("//*/*/@gmail.com").
                build();
        postSimuacaoApi.createSimulacaoInvalida(expectedSimulacao);
    }
}
