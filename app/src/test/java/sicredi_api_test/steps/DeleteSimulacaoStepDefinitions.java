package sicredi_api_test.steps;

import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import sicredi_api_test.support.api.simulacao.DeleteSimulacaoApi;
import sicredi_api_test.support.api.simulacao.GetSimulacaoApi;
import sicredi_api_test.support.api.simulacao.PostSimuacaoApi;
import sicredi_api_test.support.domain.Simulacao;

public class DeleteSimulacaoStepDefinitions {

    private Simulacao expectedSimulacao;
    private GetSimulacaoApi getSimulacaoApi;
    private DeleteSimulacaoApi deleteSimulacaoApi;

    public DeleteSimulacaoStepDefinitions() {
        getSimulacaoApi = new GetSimulacaoApi();
        deleteSimulacaoApi = new DeleteSimulacaoApi();
    }

    @Quando("pesquiso uma simulacao pelo CPF e pego o ID")
    public void pesquiso_uma_simulacao_pelo_cpf_e_pego_o_id() {
        expectedSimulacao = Simulacao.builder().build();
        getSimulacaoApi.setIdByCpf(expectedSimulacao);
    }

    @Entao("excluo a simulacao pelo ID")
    public void excluo_a_simulacao_pelo_id() {
        deleteSimulacaoApi.deletarSimulacaoById(expectedSimulacao);
    }

    @Entao("o sistema nao deve apresentar mais a simulacao na pesquisa")
    public void o_sistema_nao_deve_apresentar_mais_a_simulacao_na_pesquisa() {
        getSimulacaoApi.findSimulacaoNaoCadastradaByCpf(expectedSimulacao);
    }

}
