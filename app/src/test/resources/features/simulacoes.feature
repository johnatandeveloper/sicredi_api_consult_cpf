  # language: pt
  Funcionalidade: Gerenciamento de simulacoes

    Como um usuário (sistema de inserção)
    Quero inserir uma nova simulação
    Para simular um contrato de crédito

  Cenario: Criar nova simulacao
    Quando crio uma simuacao
    Entao devo pesquisar e verificar se o CPF esta cadastrado

  Cenario: Nao salvar simulacao com CPF duplicado
    Quando nao consigo criar uma simulacao com CPF ja cadastrado
    Entao  devo pesquisar e verificar se o CPF esta cadastrado

  Cenario: Excluir simulacao por ID
    Quando pesquiso uma simulacao pelo CPF e pego o ID
    Entao  excluo a simulacao pelo ID
    E o sistema nao deve apresentar mais a simulacao na pesquisa

  Cenario: Nao salvar com cpf invalido
    Quando nao consigo criar uma simulacao com CPF invalido
    Entao  devo pesquisar e verificar se a simulacao nao foi cadastrado

  Cenario: Nao salvar com email invalido
    Quando nao consigo criar uma simulacao com email invalido
    Entao devo pesquisar e verificar se a simulacao nao foi cadastrado



