# Automação da API Sicredi usando BDD

### Requisitos:
* Java 14
* Gradle 6.7


### Executar os Testes Localmente
* Rodar os testes - `gradlew test`
* Relatório do Cucumber - `app/build/reports/feature.html`


